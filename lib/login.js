const webdriver = require('selenium-webdriver');
const {waitUntilElementLoadByXPath} = require('./utils');

const login = async (driver, testCounter) => {

    //// Fail if no driver provided by main method
    testCounter.taken++;
    if (!driver){
        throw new Error('ERROR: No driver was provided to login function.');
    }

    //// Attempt to load the home page of the careportal-qa environment
    console.info(`\n\uD83E\uDD29 clarifyweb20 Selenium driver is initialized! Attempting to load QA Portal...`);
    await driver.get('https://qa.zerigohealth.com/login');
    // This function is good for finding static elements after a fresh pageload from "driver.get()"
    const titleElement = await driver.findElement(webdriver.By.className("loginLogoTitle"));
    const title = await titleElement.getAttribute("innerText");
    if (!title || title !=="Zerigo"){
        throw new Error(`\u274C ERROR: Failed when loading site. Expected to detect site title of 'Zerigo',
        instead recieved value '${title}'...`);
    }

    //// Find the login prompt so we can log in
    console.info(`\u2705 Care Portal QA has loaded! Attempting to find login prompt...`);
    const emailElement = await driver.findElement(webdriver.By.name("username"));
    const passwordElement = await driver.findElement(webdriver.By.name("password"));
    const loginButtonXPath = '/html/body/div[1]/div/div/div/div[1]/div[7]/div/div/button/div/div/span';
    const loginButton = await driver.findElement(webdriver.By.xpath(loginButtonXPath));
    if (!emailElement || !passwordElement || !loginButton){
        throw new Error(`\u274C ERROR: Failed when loading site. Didn't find the login prompt...`);
    }

    //// Send login request
    console.info(`\u2705 Login prompt has been found! Attempting to log in...`);
    await emailElement.sendKeys('cp@cp.com');
    await passwordElement.sendKeys('cp');
    await loginButton.click();


    //// Wait for element to load using imported Selenium fluentWait based functions(https://riptutorial.com/selenium-webdriver/example/25947/fluent-wait)
    //// Selenium fluentwait is superior to older 'implicit/explicit' timeout style wait because it is event driven and can fail gracefully.
    console.info(`\u2705 Request to log in sent! Waiting for request response...`);
    const usernameXPath = '/html/body/div/div/div/div/div/div[2]/div[1]/div/div/div[2]';
    // This 'fluentwait' function is good for finding elements after things like onclick events, when you don't want to use driver.get
    const isLoadedResult = await waitUntilElementLoadByXPath(driver, usernameXPath);
    if (!isLoadedResult){
        throw new Error(`\u274C ERROR: Failed when loading site. Didn't receive expected response after trying to log in...`);
    }
    
    //// Return to main method since login logic is completed.
    console.info(`\u2705 Server has logged us in! loading next test...`);
    testCounter.passed++;
    return true;
}

module.exports = {login}