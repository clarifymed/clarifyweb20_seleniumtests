// SAMPLE TEMPLATE TEST FOR QA
const validateSample = async (driver, testCounter) => {
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Sample Test', // TEST NAME
        true,
        '/html/body/div/div/div/div/div/div/div[1]/a[6]/div/span/div/div', // XPATH FOR ELEMENT TO CLICK
        '/html/body/div[1]/div/div/div/div/div[2]/div[2]/div/div[1]/div[1]', // XPATH FOR ELEMENT TO VALIDATE
        'Physicians'); // EXPECTED INNER TEXT FOR VALIDATION ELEMENT
    return test;
}