
const webdriver = require('selenium-webdriver');
const { until } = require('selenium-webdriver');

const waitUntilElementLoadByClassName = async (driver, _classname) => {
    try{
        await driver.wait(await until.elementLocated(await webdriver.By.className(_classname)), 21000, 'Timed out after 20 seconds', 5000);
        return true;
    }catch(error){
        return false
    }
};
const waitUntilElementLoadByXPath = async (driver, _xpath) => {
    try{
        await driver.wait(await until.elementLocated(await webdriver.By.xpath(_xpath)), 21000, 'Timed out after 20 seconds', 5000);
        return true;
    }catch(error){
        return false
    }
};

const withSearchParams = (byXPath, pathOrClass) => {
    if (byXPath){
        return webdriver.By.xpath(pathOrClass);
    }
    return webdriver.By.xpath(pathOrClass);
};

/**
 * buildAndExecuteTestCase
 * 
 * Build test cases based on the provided parameters. Allows for clean coverage of event-driven html content.  
 * This function should be used whenevr you want to validate that HTML is rendered as expected after clicking different
 * parts of the portal (Ie: Clicking button on patients tab; Clicking a name link to show details of specific patient).
 * 
 * This builder is not designed to support text input submissions or form completetion. I would reccomend hand-writing test
 * cases to cover scenarios like that.
 * @param {*} driver 
 * @param {*} testCounter 
 * @param {*} testName 
 * @param {*} useXPath 
 * @param {*} elementOneLocator 
 * @param {*} elementTwoLocator 
 * @param {*} elementTwoExpectedValue 
 * @returns Boolean result -- Outcome of the created test cases execution
 */
 const buildAndExecuteTestCase = async (driver, testCounter, testName, useXPath, elementOneLocator, elementTwoLocator, elementTwoExpectedValue) => {
    
    // Increment total test count before running tests. Also validate params.
    testCounter.taken++;
    console.info(`\n\uD83E\uDD29 New test case loaded! Validating **${testName}** params...`);
    if (!driver || !testCounter || !testName || useXPath === undefined || useXPath === null || !elementOneLocator || !elementTwoLocator){
        console.info(`\u274C Failed ${testName}. Did not find all required params.`);
        return false;
    }

    // Locate the parent element of the onClick event, which loads tested content
    console.info(`\u2705 Params validated! Finding element to click for loading **${testName}** desired content...`);
    const onClickParentElement = await driver.findElement(withSearchParams(useXPath, elementOneLocator));
    if (!onClickParentElement){
        console.info(`\u274C Failed case ${testName}. Did not find parent element for onClick eventß.`);
        return false;
    }
    
    // Trigger the onClick event and await confirmation that content loaded
    console.info(`\u2705 onClick parent element found! Attempting to trigger the onClickEvent for **${testName}** and load content...`);
    await onClickParentElement.click();
    const isLoadedResult = await waitUntilElementLoadByXPath(driver, elementTwoLocator);
    if (!isLoadedResult){
        console.info(`\u274C Failed case ${testName}. Did not see expected content loaded after onClick event.`);
        return false;
    }

    // Check the text inside the loaded content and make sure it matches what was expected
    console.info(`\u2705 onClick successful and content loaded! Validating **${testName}** inner text...`);
    const validationElement = await driver.findElement(withSearchParams(useXPath, elementTwoLocator));
    const innerText = await validationElement.getAttribute("innerText");
    // Validate that innerText is populated as expected. If innerText is not as expected, fail this test case.
    if (!innerText || innerText !== elementTwoExpectedValue){
        console.info(`\u274C Failed case ${testName}. Expected to detect innerText of ${elementTwoExpectedValue},
        instead recieved value '${innerText}'`);
        return false;
    }

    // Update passed test count and return to calling function
    console.info(`\u2705 Finished validating **${testName}** innerText! Loading next test...`);
    testCounter.passed++;
    return true;
}

module.exports = {waitUntilElementLoadByXPath, waitUntilElementLoadByClassName, buildAndExecuteTestCase}