const {buildAndExecuteTestCase} = require('./utils');

const validateLandingPage = async (driver, testCounter) => {
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Landing Page',
        false,
        '/html/body/div/div/div/div/div/div[1]/a[2]/div/span/div',
        '/html/body/div/div/div/div/div/div[2]/div[1]/div/div/div[2]',
        'Nicole Keefer');
    return test;
}

const validateAlerts = async (driver, testCounter) => {
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Alerts Test',
        true,
        '/html/body/div/div/div/div/div/div[1]/a[2]/div/span/div/div/img',
        '/html/body/div/div/div/div/div/div[2]/div[2]/div/div[1]/div[1]',
        'Alerts');
    return test;
}

const validatePatients = async (driver, testCounter) => {
    
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Patients Tab Test',
        true,
        '/html/body/div/div/div/div/div/div[1]/a[3]',
        '/html/body/div/div/div/div/div/div/div[2]/div[2]/div/div[1]/div[1]',
        'Patients');
    return test;
}


const validateMessages = async (driver, testCounter) => {
    
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Messages Tab Test',
        true,
        '/html/body/div/div/div/div/div/div/div[1]/a[4]/div/span/div/div',
        '/html/body/div[1]/div/div/div/div/div[2]/div[2]/div/div[1]/div',
        'Messages');
    return test;
}

const validateProtocols = async (driver, testCounter) => {
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Protocols Tab Test',
        true,
        '/html/body/div[1]/div/div/div/div/div[1]/a[5]/div/span/div/div/div',
        '/html/body/div/div/div/div/div/div/div[2]/div[2]/div/div[1]/div',
        'Protocols');
    return test;
}

const validatePhysicians = async (driver, testCounter) => {
    const test = await buildAndExecuteTestCase(
        driver,
        testCounter,
        'Physicians Tab Test',
        true,
        '/html/body/div/div/div/div/div/div/div[1]/a[6]/div/span/div/div',
        '/html/body/div[1]/div/div/div/div/div[2]/div[2]/div/div[1]/div[1]',
        'Physicians');
    return test;
}

module.exports = {validateLandingPage, validateAlerts, validatePatients, validateMessages, validateProtocols, validatePhysicians}