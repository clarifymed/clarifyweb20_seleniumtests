const webdriver = require('selenium-webdriver');
const driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
const {login} = require('./lib/login');
const {validateLandingPage, validateAlerts, validatePatients, validateMessages, validateProtocols, validatePhysicians} = require('./lib/tests')

/**
 * Main method for our Selenium scripts for testing Care Portal. Uses a Chrome instance to get to QA portal,
 * log in, and validate desired functionalities. 
 * 
 * The driver instance can be used to grab elements by their className (only use classname if it is unique) or by
 * their xpath (ideal, assuming that developers remember to update the paths if JSX/HTML ever changes). We currently
 * do not support grabbing elementsbyid as our current app often generates different ids for DOM elements on each load.
 * 
 */
const main = async () => {

  const testCounter = {passed: 0, taken: 0};
    try {
      // Attempt to load Care Portal QA Environment. Must be done before beginning other tests
      await login(driver, testCounter);

      // Run individual tests. Currently they need to all be ran in sequence because xpath changes by context.
      // These should be invoked with a promise.allSettled() for better performance after dev team gives testElements unique ids.
      await validateLandingPage(driver, testCounter);
      await validateAlerts(driver, testCounter);
      await validatePatients(driver, testCounter);
      await validateMessages(driver, testCounter);
      await validateProtocols(driver, testCounter);
      await validatePhysicians(driver, testCounter);


      console.info('\u2705\u2705\u2705 All test cases attempted successfully! Exiting program...');
      
    } catch (error) {
      
      console.info('\u274C Failed to complete test run. You can see the exception details below. Preparing to exit program...');
      console.error(error);
      
    } finally {

        // Terminate Selenium when work is done, regardless of whether execution succeeded or not.
        const {passed, taken} = testCounter;
        const result = `\n\n${passed === taken? '\u2705' : '\u274C'} RESULT: In this test run, selenium finished ${passed}/${taken} attempted test cases!\n`;
        console.info(result);
        driver.close();
        driver.quit();
        return {testCounter, result};
    }
  };

  main();
  